package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private HashMap<String, Double> bookPrices;

    public Quoter() {
        this.bookPrices = new HashMap<>();
        initializePricesMap();
    }

    public void initializePricesMap() {
        bookPrices.put("1", 10.0);
        bookPrices.put("2", 45.0);
        bookPrices.put("3", 20.0);
        bookPrices.put("4", 35.0);
        bookPrices.put("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        return bookPrices.containsKey(isbn)? bookPrices.get(isbn): 0.0;
    }
}
